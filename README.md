# Boomar – simple command line bookmark manager

Manages a list of paths to ebook files and opens a file on a given page.

Requires Ruby.

Default command opens file with Preview.app on macOS and uses cliclick to go to page which requires installing cliclick, e.g. `brew install cliclick`.

Location of the list and the command can be configured in the _Configuration_ section at the beginning of the script.

`./boomar.rb book.pdf` adds book to open at page 1.

`./boomar.rb book.pdf:12` adds book and/or updates it to open at page 12.

`./boomar.rb book.pdf:x` removes the book from the list.

`./boomar.rb` shows a list of books and waits for a command can be used but with index substituted for path to the book. Use `q` to save the list and exit or Control-C to abort all changes and exit.