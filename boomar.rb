#! /usr/bin/env ruby
# Boomar – simple bookmark manager
#
# Manages a list of ebooks and 
# opens an ebook a given page.

# Paths are saved in the following format:
# path_to_file:page_number
# 
# Only one page number is allowed for each ebook

# Configuration
# 
# Location of file containing the list
# Default is libary.txt in the same directory as the script
@libary = File.join(__dir__, "libary.txt")
# Shell command to open an ebook on given page
# First %s specifies the path to ebook, second %s specifies the page number.
# Default opens file with Preview.app on macOS and uses cliclick to go to page.
# (Requires installing cliclick, e.g. `brew install cliclick`)
@open_action = 'open "%s";cliclick w:500 kd:cmd,alt t:g ku:cmd,alt t:%s kp:enter'

@record_colour = ["\u001b[36m", "\u001b[32m"].cycle
@record_colour.next if rand(2)==1
@boomars = {}

def read_boomars
  boomars = {}
  File.read(@libary).each_line do |l|
    booke = l.chomp.split(':')
    boomars[booke[0]] = booke[1]
  end
  boomars
end

def update_boomars(boomars)
  File.open(@libary, 'w') do |f|
    boomars.each do |k, v|
      f.puts("#{k}:#{v}")
    end
  end
end

def open(filename, page)
  if File.exist? filename
    system(@open_action % [filename, page])
    STDERR.puts "\u001b[34mopened #{@record_colour.next}#{record}\u001b[0m @ \u001b[33m#{pagenum}\u001b[0m"
  else
    STDERR.puts "\u001b[31mnot there\u001b[0m"
  end
end

def act(booke)
  record = booke[0]
  if booke.size<2
    pagenum = nil
  else
    pagenum = booke[1]
  end
  
  if @boomars.has_key?(record)
    if pagenum
      if pagenum=='x'
        # delete
        @boomars.delete(booke[0])
        STDERR.puts "\u001b[34mdeleted #{@record_colour.next}#{booke[0]}\u001b[0m"
      else
        if pagenum.to_i <= 0
          # nonsensical page number
          STDERR.puts("\u001b[31mnonsensical page number\u001b[0m")
          STDERR.puts "\u001b[34mskipped #{@record_colour.next}#{record}\u001b[0m"
        else
          # update
          @boomars[record] = pagenum
          STDERR.puts "\u001b[34mupdated #{@record_colour.next}#{record}\u001b[0m @ \u001b[33m#{pagenum}\u001b[0m"
        end
      end
    else
      # open
      pagenum = @boomars[record]
      open(record, pagenum)
    end
  else
    if pagenum
      if pagenum=="x"
        # nothing to delete
        STDERR.puts("\u001b[31mnothing to delete\u001b[0m")
        STDERR.puts "\u001b[34mskipped #{@record_colour.next}#{record}\u001b[0m"        
      else
        if pagenum.to_i <= 0
          # nonsensical page number
          STDERR.puts("\u001b[31mnonsensical page number\u001b[0m")
          STDERR.puts "\u001b[34mskipped #{@record_colour.next}#{record}\u001b[0m"
        else
          # add at given page
          @boomars[record] = pagenum
          STDERR.puts "\u001b[34madded #{@record_colour.next}#{record}\u001b[0m @ \u001b[33m#{pagenum}\u001b[0m"
        end
      end
    else
      # add at page one
      pagenum = 1
      @boomars[record] = pagenum
      STDERR.puts "\u001b[34madded #{@record_colour.next}#{record}\u001b[0m @ \u001b[33m#{pagenum}\u001b[0m"      
    end
  end
end

def quit
  update_boomars(@boomars)
  STDERR.puts "\u001b[34mbye\u001b[0m"
  exit
end

def next_or_red_colour(filename)
  if File.exist? filename
    @record_colour.next
  else
    "\u001b[31m"
  end
end

def choose
  begin
    while true
      keys = @boomars.keys
      
      keys.each_with_index do |k, i|
        STDERR.puts "[\u001b[35m#{i+1}\u001b[0m] #{next_or_red_colour(k)}#{k}\u001b[0m @ \u001b[33m#{@boomars[k]}\u001b[0m"    
      end
      choice = gets.chomp
      if choice=='q'
        quit
      else
        booke = choice.split(':')
        indexe = booke[0].to_i
        if (indexe>0 && indexe<=keys.size)
          booke[0] = keys[indexe-1]
          act(booke)
        else
          STDERR.puts("\u001b[31mchoose again\u001b[0m")
        end
      end
    end
  rescue Interrupt
    STDERR.puts("\n\u001b[31mnothing saved\u001b[0m")
  end
end

@boomars = read_boomars
if ARGV.empty?
  choose
else
  ARGV.each do |a|
    act(a.split(':'))
  end
  quit
end
